<?php
// namespace Tests\Integration;

use PHPUnit\Framework\TestCase;

require_once 'src/class/disc.php';
require_once 'src/class/tower.php';

class TowerIntegrationTest extends TestCase
{
    public function testPushAndPop()
    {
        $tower = new Tower();
        $this->assertTrue($tower->isEmpty());

        $disc = new Disc(3);
        $tower->push($disc);

        $this->assertFalse($tower->isEmpty());
        $this->assertEquals($disc, $tower->peek());

        $poppedDisc = $tower->pop();
        $this->assertEquals($disc, $poppedDisc);
        $this->assertTrue($tower->isEmpty());
    }

    public function testIsFull()
    {
        $tower = new Tower(true);
        $this->assertTrue($tower->isFull());

        $tower->pop();
        $this->assertFalse($tower->isFull());
    }
}