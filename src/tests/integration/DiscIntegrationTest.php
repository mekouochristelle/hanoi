<?php
// namespace Tests\Integration;

use PHPUnit\Framework\TestCase;

class DiscIntegrationTest extends TestCase
{
    public function testDiscSize()
    {
        $disc = new Disc(3);
        $this->assertEquals(3, $disc->size());
    }

    public function testDiscIsGreaterThan()
    {
        $disc1 = new Disc(5);
        $disc2 = new Disc(3);
        $this->assertTrue($disc1->isGreaterThan($disc2));
        $this->assertFalse($disc2->isGreaterThan($disc1));
    }
}