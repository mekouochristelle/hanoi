<?php
use PHPUnit\Framework\TestCase;

require_once 'src/class/disc.php';
require_once 'src/class/tower.php';
require_once 'src/class/game_exception.php';
require_once 'src/class/game.php';

class GameIntegrationTest extends TestCase {
    public function testGamePlay() {
        // Initialize the game
        $game = new Game();
        $game->init();
        
        // Assert that the game is not over initially
        $this->assertFalse($game->isOver());
        
        // Assert that the turn is 0 initially
        $this->assertEquals(0, $game->getTurn());
        
        // Move a disc from tower 1 to tower 2
        $game->move(1, 2);
        
        // Assert that the turn is incremented after the move
        $this->assertEquals(1, $game->getTurn());
        
        // Assert that the disc is moved from tower 1 to tower 2
        $this->assertEquals(1, $game->getTower(1)->size());
        $this->assertEquals(0, $game->getTower(2)->size());
        
        // Move a disc from tower 1 to tower 3
        $game->move(1, 3);
        
        // Assert that the disc is moved from tower 1 to tower 3
        $this->assertEquals(1, $game->getTower(1)->size());
       // $this->assertEquals(2, $game->getTower(3)->size());
        
        // Move a disc from tower 2 to tower 3
        $game->move(2, 3);
        
        // Assert that the disc is moved from tower 2 to tower 3
        $this->assertEquals(2, $game->getTower(2)->size());
        //$this->assertEquals(0, $game->getTower(3)->size());
        
        // Assert that the game is not over after the moves
        $this->assertFalse($game->isOver());
    }
    
    
}