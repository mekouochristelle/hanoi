<?php
use PHPUnit\Framework\TestCase;

require_once 'src/class/disc.php';
require_once 'src/class/tower.php';
require_once 'src/class/game_exception.php';
require_once 'src/class/game.php';


class GameTest extends TestCase
{
    protected $game;

    protected function setUp(): void
    {
        $this->game = new Game();
        $this->game->init();
    }

    public function testMoveValid()
    {
        // Test a valid move
        $this->game->move(1, 2);
        $this->assertEquals(1, $this->game->getTurn());
        // Add more assertions to validate the state of the game after the move
    }

    public function testMoveInvalid()
    {
        // Test an invalid move
        $this->expectException(GameException::class);
        $this->game->move(2, 1);
    }


    // Ttest  the isOver() method
    public function testIsOver()
    {
        $this->assertFalse($this->game->isOver());
    }


    public function testInit()
    {
        // Test the initialization of the game
        $this->game->init();
        $this->assertEquals(0, $this->game->getTurn());
        $this->assertFalse($this->game->isOver());
        // Add more assertions to validate the state of the game after initialization
    }
    
    public function testMoveFromInvalidTower()
    {
        // Test moving from an invalid tower
        $this->expectException(Exception::class);
        $this->game->move(0, 2);
    }
    
    public function testMoveToInvalidTower()
    {
        // Test moving to an invalid tower
        $this->expectException(Exception::class);
        $this->game->move(1, 4);
    }
    
    public function testMoveFromEmptyTower()
    {
        // Test moving from an empty tower
        $this->expectException(GameException::class);
        $this->game->move(2, 1);
    }
    
    public function testMoveNotAllowed()
    {
        // Test moving a larger disc onto a smaller disc
        $this->expectException(GameException::class);
        $this->game->move(1, 2);
        $this->game->move(1, 2);
    }
    
    public function testGetTower()
    {
        // Test getting a tower object
        $tower = $this->game->getTower(0);
        $this->assertInstanceOf(Tower::class, $tower);
        
    }
    
}


