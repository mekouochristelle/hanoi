<?php declare(strict_types=1);

require_once 'src/class/disc.php';

class DiskTest extends PHPUnit\Framework\TestCase
{
    public function testDiscSize()
    {
        $disc = new Disc(3);
        $this->assertEquals(3, $disc->size());
    }

    public function testIsGreaterThan()
    {
        $disc1 = new Disc(4);
        $disc2 = new Disc(2);

        $this->assertTrue($disc1->isGreaterThan($disc2));
        $this->assertFalse($disc2->isGreaterThan($disc1));
    }
} 