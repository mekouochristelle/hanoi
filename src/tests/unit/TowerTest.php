<?php declare(strict_types=1);


require_once 'src/class/disc.php';
require_once 'src/class/tower.php';

class TowerTest extends PHPUnit\Framework\TestCase
{
    public function testPushAndPop()
    {
        $tower = new Tower();
        $disc = new Disc(3);

        $tower->push($disc);
        $this->assertEquals($disc, $tower->pop());
    }

    public function testIsEmpty()
    {
        $tower = new Tower();
        $this->assertTrue($tower->isEmpty());

        $disc = new Disc(4);
        $tower->push($disc);
        $this->assertFalse($tower->isEmpty());
    }

    public function testIsFull()
    {
        $tower = new Tower();
        $this->assertFalse($tower->isFull());

        for ($i = 1; $i <= 7; $i++) {
            $tower->push(new Disc($i));
        }

        $this->assertTrue($tower->isFull());
    }

    public function testPeek()
    {
        $tower = new Tower();
        $disc = new Disc(5);
        $tower->push($disc);

        $this->assertEquals($disc, $tower->peek());
    }

    public function testSize()
    {
        $tower = new Tower();
        $this->assertEquals(0, $tower->size());

        $tower->push(new Disc(2));
        $tower->push(new Disc(4));

        $this->assertEquals(2, $tower->size());
    }
}


// class TowerTest extends PHPUnit\Framework\TestCase {
//     public function testPush() {
//         $tower = new Tower();
//         $disc = new Disc(1);
        
//         // Assert that the tower is initially empty
//         $this->assertTrue($tower->isEmpty());
        
//         // Push a disc onto the tower
//         $tower->push($disc);
        
//         // Assert that the tower is not empty after pushing a disc
//         $this->assertFalse($tower->isEmpty());
        
//         // Assert that the pushed disc is the top disc in the tower
//         $this->assertEquals($disc, $tower->peek());
//     }
    
//     public function testPop() {
//         $tower = new Tower();
//         $disc = new Disc(1);
        
//         // Push a disc onto the tower
//         $tower->push($disc);
        
//         // Pop the disc from the tower
//         $poppedDisc = $tower->pop();
        
//         // Assert that the popped disc is the same as the pushed disc
//         $this->assertEquals($disc, $poppedDisc);
        
//         // Assert that the tower is empty after popping the disc
//         $this->assertTrue($tower->isEmpty());
//     }
    
//     // Add more test methods for the remaining Tower class methods
    
// }
